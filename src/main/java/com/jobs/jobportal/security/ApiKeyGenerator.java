package com.jobs.jobportal.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiKeyGenerator {

  @Value("${app.http.auth-token}")
  private String apiToken;

  public String getApiToken() {
    return apiToken;
  }
}
