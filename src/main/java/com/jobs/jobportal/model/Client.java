package com.jobs.jobportal.model;

import java.util.HashSet;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Client {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotBlank(message = "Name is mandatory")
  @Size(max = 100, message = "The length of the name must be between 0-100 character!")
  private String name;

  @NotBlank(message = "Email is mandatory")
  // Regular Expression by RFC 5322 standard
  @Email(message = "Email is not valid",
      regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
  private String email;

  public Client(long id, String name, String email) {
    super();
    this.id = id;
    this.name = name;
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int numUniqueEmails(String[] emails) {
    HashSet<String> emailSet = new HashSet<>();
    for (String email : emails) {
      String firstSplit[] = email.split("@");
      String secondSplit[] = firstSplit[0].replaceAll(".", "").split("[+]");
      emailSet.add(secondSplit[0] + firstSplit[1]);
    }
    return emailSet.size();
  }
}
