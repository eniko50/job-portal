package com.jobs.jobportal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
public class Position {
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;
  @NotBlank(message = "Name is mandatory!")
  @Size(max = 50, message = "The length of the name must be between 0-50 character!")
  private String name;
  @NotBlank(message = "Location is mandatory!")
  @Size(max = 50, message = "The length of the location name must be between 0-50 character!")
  private String location;
  private String content;

  public Position(long id, String name, String location, String content) {
    this.id = id;
    this.name = name;
    this.location = location;
    this.content = content;
  }

  public Position(long incrementAndGet, String format) {
    this.id = incrementAndGet;
    this.content = format;
  }

  public Position() {

  }

  public long getId() {
    return id;
  }

  public String getContent() {
    return content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setId(long id) {
    // TODO Auto-generated method stub
    this.id = id;
  }
}
