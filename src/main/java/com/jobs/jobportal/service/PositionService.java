package com.jobs.jobportal.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.jobs.jobportal.data.PositionRepository;
import com.jobs.jobportal.model.Position;

@Component
public class PositionService {

  @Autowired
  PositionRepository positionRepositiory;

  public Position findOne(int id) {
    return positionRepositiory.findOne(id);
  }

  public List<Position> findAll() {
    return positionRepositiory.findAll();
  }
  
  public List<Position> findByNameOrLocation(String name, String location) {
    return positionRepositiory.findByNameOrLocation(name, location);
  }

  public Position save(Position position) {
    return positionRepositiory.save(position);
  }

}
