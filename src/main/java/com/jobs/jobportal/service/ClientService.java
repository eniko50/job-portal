package com.jobs.jobportal.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.jobs.jobportal.data.ClientRepository;
import com.jobs.jobportal.model.Client;

@Component
public class ClientService {
  
  @Autowired
  ClientRepository clientRepository;
  
  public Client findOne(long id) {
    return clientRepository.findOne(id);
  }

  public List<Client> findAll() {
    return clientRepository.findAll();
  }

  public Client save(Client client) {
    return clientRepository.save(client);
  }

}
