package com.jobs.jobportal.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.jobs.jobportal.model.Client;
import com.jobs.jobportal.security.ApiKeyGenerator;
import com.jobs.jobportal.service.ClientService;

@RestController
public class ClientController {

  @Autowired
  ClientService clientService;
  @Autowired
  ApiKeyGenerator apiKeyGenerator;

  @GetMapping("/api/clients")
  public ResponseEntity<List<Client>> getAllCleits(
      @RequestParam(defaultValue = "No clients were found!", name = "name") String name,
      Pageable page) {
    List<Client> clients = clientService.findAll();

    return new ResponseEntity<List<Client>>(clients, HttpStatus.OK);
  }

  @PostMapping("/api/client")
  public ResponseEntity<String> postClient(@Valid @RequestBody Client client) {

    clientService.save(client);
    try {
      String apiKey = apiKeyGenerator.getApiToken();
      return new ResponseEntity<String>(apiKey, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<String>("Invalid API key", HttpStatus.BAD_REQUEST);
    }
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }
}
