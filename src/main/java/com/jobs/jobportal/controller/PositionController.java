package com.jobs.jobportal.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.jobs.jobportal.model.Position;
import com.jobs.jobportal.service.PositionService;

@RestController
public class PositionController {

  @Autowired
  PositionService positionService;

  @Value("${server.port}")
  String port;

  @Value("${server.host}")
  String host;

  @GetMapping("/api/positions")
  public ResponseEntity<List<Position>> getAllPositions(
      @RequestParam(defaultValue = "No positions were found!", name = "name") String name,
      Pageable page) {
    List<Position> positions = positionService.findAll();

    return new ResponseEntity<List<Position>>(positions, HttpStatus.OK);
  }


  // http://localhost:8080/api/positions/search?name=Dev&location=New%20York
  @RequestMapping(value = "/api/position/search", method = RequestMethod.GET)
  public ResponseEntity<List<Position>> getPositionssByNameAndLocation(
      @RequestParam(value = "name", required = true) String name,
      @RequestParam(value = "location", required = false) String location,
      @RequestParam Map<String, String> req) {
    List<Position> positions = positionService.findByNameOrLocation(name, location);

    return new ResponseEntity<List<Position>>(positions, HttpStatus.OK);
  }

  @GetMapping("/api/position/{id}")
  public ResponseEntity<Position> getPosition(@PathVariable int id) {
    Position position = positionService.findOne(id);

    return new ResponseEntity<Position>(position, HttpStatus.OK);
  }

  @PostMapping("/api/position")
  public ResponseEntity<String> postPosition(@Valid @RequestBody Position position) {

    Position retVal = positionService.save(position);
    String response = "Your created position is available at: http://" + host + ":" + port
        + "/api/position/" + retVal.getId();

    return new ResponseEntity<String>(response, HttpStatus.CREATED);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });
    return errors;
  }
}
