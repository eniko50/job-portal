package com.jobs.jobportal.data;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.jobs.jobportal.model.Client;

@Component
public class ClientRepository {

  private List<Client> clients = new ArrayList<Client>();
  

  public ClientRepository() {
    super();
    clients.add(new Client(1, "eniko50", "eniko50@gmail.com"));
    clients.add(new Client(2, "eniko51", "eniko51@gmail.com"));
    clients.add(new Client(3, "eniko52", "eniko52@gmail.com"));
  }

  public List<Client> findAll() {
    return clients;
  }

  public Client findOne(long id) {
    for (Client client : clients) {
      if (client.getId() == id) {
        return client;
      }
    }
    return null;
  }

  public Client save(Client client) {
    
    Client existingClient = findOne(client.getId());
    if (existingClient == null) {
        client.setId(clients.size() + 1);
        clients.add(client);
    } 
    return client;
}


}
