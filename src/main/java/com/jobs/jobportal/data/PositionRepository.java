package com.jobs.jobportal.data;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.jobs.jobportal.model.Position;

/**
 * Az osztály a pozíciók perzisztálásáért felelős. Az egyszerűség kedvéért, az adatokat a munka
 * memóriában tároljuk, nem az adatbázisban.
 * 
 * @author Eniko Juhasz
 *
 */
@Component
public class PositionRepository {
  private List<Position> positions = new ArrayList<Position>();

  // a pédányosítást követően feltoltodik in-memory teszt adatokkal
  public PositionRepository() {
    positions.add(new Position(1, "Java developer", "Budapest", "Lorem ipsum"));
    positions.add(new Position(2, "DevOps Engineer", "New York", "Dolor sit amen!"));
    positions.add(new Position(3, "Data Analyst", "Budapest", "Lorem ipsum, dolor sit amen!"));
  }


  public List<Position> findAll() {
    return positions;
  }

  public Position findOne(long id) {
    for (Position position : positions) {
      if (position.getId() == id) {
        return position;
      }
    }
    return null;
  }

  public List<Position> findByNameOrLocation(String name, String location) {
    List<Position> retVal = new ArrayList<>();
    for (Position position : positions) {
      if (position.getName().contains(name) && position.getLocation().contains(location)) {
        retVal.add(position);
      }
    }
    return retVal;
  }

  public Position save(Position position) {

    Position existingPosition = findOne(position.getId());
    if (existingPosition == null) {
      position.setId(positions.size() + 1);
      positions.add(position);
    } 
    return position;
  }
}
