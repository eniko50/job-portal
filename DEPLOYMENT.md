# Job portal API web application

## Build application
Requirements:
* Temurin OpenJDK 11.0.15
* Maven 3.8+

`job-portal` uses Maven 3.8+ for all software project management purposes. Run `mvn package` to build the `.jar` archive. It will be available in the target directory.
  
## Overview of components

### 1. Job Portal API web application
The job-portal a web application API designed for client applications in order to create and search job vacancies.
#### 1.1. Implementation info 
job-portal was implemented in Java and tested to run on Apache Tomcat/9.0.65. Postman client is used for testing the API functionality.
It uses API key for authentication.
#### 1.2. application.properties file 
The application.properties is the main configuration file. To override the default configuration and run the apllication with it use the command below.

`java -jar -Dspring.config.location=<path-to-file> job-portal-0.0.1-SNAPSHOT.jar`

## 2. Installation steps
### <a name="config-example"></a>Example application configuration
```
# The key name for the API key
app.http.auth-token-header-name=X-API-KEY

# The API key used for testing purposes. 
# For running on test and production environment this value must be generated, different for test and for production and cannot be shared here.
app.http.auth-token=8e4ba7a4-c5bb-4a03-b93b-6a8a5c298818

# The default server port where the apllication is configured to run
server.port=8080

# The default server IP where the application is configured to run
server.host=127.0.0.1
```
## Run the appliation
To run the application with the default configuration use the command:

`java -jar job-portal-0.0.1-SNAPSHOT.jar`

After that the application is available on http://localhost/api
### Description of features
#### 1. Client registration
In Postman use the POST method to add a new client on address `http://localhost:8080/api/client`. The `name` and `email` field are required to be added. Set Postman body field to accept JSON. An example to register the new client:

![image](https://user-images.githubusercontent.com/61797571/183912597-31f48ea4-517f-4996-8676-3b6869c45242.png)

If the data was valid, the apllication should return the API key.
#### 2. Add a new position
In Postman use POST method to add a new position on  address `http://localhost:8080/api/position`.
A position can be added if the client has a valid API key. Set Postman to accept API key authorization as in the picture below. 
Use the test API key from the example configuration.

![image](https://user-images.githubusercontent.com/61797571/183915340-e4316eb2-39e4-46e0-81d4-e143d70937f9.png)

The `name` and `location` fileds are required to be added. Set Postman body field to accept JSON. An example to add a new position:

![image](https://user-images.githubusercontent.com/61797571/183918085-f53780ac-1ffa-4c7a-8285-3e03381b2efa.png)

After that the position will be available on the URL sent in the response by using the GET method in Postman.

#### 3. Searching positions
The apllication provides feature for searching positions for the authenticated users. Please use the API key in Posman authorization header as in the picture above.
To search a new postion use the GET method in Postman `http://localhost:8080/api/position/search?name={positionName}&location={positionLocation}`, where the `positionName` and `positionLocation` should be replaced by the searched position name and location. An example of search:

![image](https://user-images.githubusercontent.com/61797571/183920320-3dbd0898-d193-485e-8870-0e04854c0f4d.png)
